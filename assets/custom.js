if (window.matchMedia("(max-width: 767px)").matches) {
jQuery('ul.exceeding_icon').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    infinite: true,
});
}


$(document).ready(function(){
  $(document).on('click', '.accordion-title', function() {
    var $item = $(this).closest('.accordion-item');
    if($item.hasClass('open')) {
      $item.find('.accordion-content').slideToggle();
    }
    else {
      $('.accordion-item .accordion-content').slideUp();
      $('.accordion-item.open').removeClass('open');
      $item.find('.accordion-content').slideDown();
    }
    $item.toggleClass('open');
  })
})